
FROM php:7.4.23-fpm-alpine3.13 as base
 
ENV WORK_DIR /home/vzhakulin/Рабочий стол/code/gitrem/okay/okok
 
 
FROM base
 
COPY . ${WORK_DIR}
 
 
EXPOSE 9000
CMD ["php-fpm"]
