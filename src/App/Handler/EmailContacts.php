<?php

declare(strict_types=1);

namespace App\Handler;
use Laminas\Diactoros\Response\HtmlResponse;
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\Diactoros\Response\RedirectResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Unisender\ApiWrapper\UnisenderApi;

use function time;

class EmailContacts implements RequestHandlerInterface
{
    
    public function handle(ServerRequestInterface $request): ResponseInterface
    {

        
        $ApiKey = '6n1drxnyc8bsowee9fooross433inogowp3oskoy';
        $uni = new UnisenderApi(ApiKey, 'UTF-8', 4, null, false);
        
        $clientId = '79bf931d-bd2d-4adc-8500-35d477ecf973';
        $clientSecret = 'wtMrxfenX4lyqoFlq7trbXZhT9RE9meWO9vfRb1m4JYTSYLSz6xHUfQbQ5QoQIme';
        $redirectURI = 'https://4c56-212-46-197-210.eu.ngrok.io/api/uni';
        
        $apiClient = new \AmoCRM\Client\AmoCRMApiClient($clientId, $clientSecret, $redirectURI);
        
        $state = bin2hex(random_bytes(16));

        if(isset($request->getQueryParams()['referer'])) {
            $apiClient->setAccountBaseDomain($request->getQueryParams()['referer']);
        }

        if(!isset($request->getQueryParams()['code'])) {
            $authorizationUrl = $apiClient->getOAuthClient()->getAuthorizeUrl([
                'state' => $state,
                'mode' => 'post_message',
            ]);
            $response = new RedirectResponse($authorizationUrl);

        } else {

            if(isset($request->getQueryParams()['code'])) {
                $accessToken = $apiClient->getOAuthClient()->getAccessTokenByCode($request->getQueryParams()['code']); 
            }
            
            if ((isset($accessToken)) and (!$accessToken->hasExpired())) {
                $saveToken = [
                    'accessToken' => $accessToken->getToken(),
                    'refreshToken' => $accessToken->getRefreshToken(),
                    'expires' => $accessToken->getExpires(),
                    'baseDomain' => $apiClient->getAccountBaseDomain(),
                ];
            }
        $apiClient->setAccessToken($accessToken);
        $cont = $contacts = $apiClient->contacts()->get()->toArray(); 
        $data = [];
        foreach($cont as $contact) {
            if(isset($contact['custom_fields_values'][1])) {
                foreach($contact['custom_fields_values'][1]['values'] as $item) {
                    $s = $this->addContact($item['value']);
                    $s = json_decode($s);
                    array_push($data, $s);
                }
            }
        }
        $response = new JsonResponse($data);
    }
        
        return new $response;
    
    }

    public function addContact($email) {
        $apikey = ApiKey;
        $a = "https://api.unisender.com/ru/api/importContacts?format=json&api_key=$apikey&field_names[0]=email&data[0][0]=$email";
        $ch = curl_init($a);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $a = curl_exec($ch);
        curl_close($ch);
        return $a;
    }
}


?>
