<?php
/*    use Unisender\ApiWrapper\UnisenderApi;

$ApiKey = '6n1drxnyc8bsowee9fooross433inogowp3oskoy';
$uni = new UnisenderApiWrapperUnisenderApi($ApiKey);

$response = $UnisenderApi->getContact(array("m00nl1ghtmwp@gmail.com"));
*/
declare(strict_types=1);

namespace App\Handler;
use Laminas\Diactoros\Response\HtmlResponse;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Unisender\ApiWrapper\UnisenderApi;

use function time;

class emailHandler implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $ApiKey = '6n1drxnyc8bsowee9fooross433inogowp3oskoy';
        $uni = new UnisenderApi(ApiKey, 'UTF-8', 4, null, false, 'XD');
        $okay = [
            'email' => 'm00nl1ghtmwp@gmail.com',
            'api_key' => ApiKey,
        ];
        $response = $uni->getContact($okay);
        return new JsonResponse($response);
    }
}
?>
