<?php

declare(strict_types=1);

namespace App\Handler;

use Laminas\Diactoros\Response\HtmlResponse;
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\Diactoros\Response\RedirectResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

use function time;

class ContactsHandler implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        
        $clientId = '3d427a2e-6c8a-4cbd-98f4-02c18ba779bf';
        $clientSecret = 'sAIMi3OVzqsz6qR5tVeaJvN4t565uWcmKeYeDMDr8oYd5XDKtZE49kxMgkwTb8PO';
        $redirectURI = 'https://4c56-212-46-197-210.eu.ngrok.io//api/contacts';
        $apiClient = new \AmoCRM\Client\AmoCRMApiClient($clientId, $clientSecret, $redirectURI);
        $state = bin2hex(random_bytes(16));
        if(isset($request->getQueryParams()['referer'])) {
            $apiClient->setAccountBaseDomain($request->getQueryParams()['referer']);
        }
        if(!isset($request->getQueryParams()['code'])) {
            $authorizationUrl = $apiClient->getOAuthClient()->getAuthorizeUrl([
                'state' => $state,
                'mode' => 'post_message',
            ]);
            $respone = new RedirectResponse($authorizationUrl);
        } else {
            if(isset($request->getQueryParams()['code'])) {
                $accessToken = $apiClient->getOAuthClient()->getAccessTokenByCode($request->getQueryParams()['code']); 
            }
            
            if ((isset($accessToken)) and (!$accessToken->hasExpired())) {
                $saveToken = [
                    'accessToken' => $accessToken->getToken(),
                    'refreshToken' => $accessToken->getRefreshToken(),
                    'expires' => $accessToken->getExpires(),
                    'baseDomain' => $apiClient->getAccountBaseDomain(),
                ];

            $ownerDetails = $apiClient->getOAuthClient()->getResourceOwner($accessToken);
            $apiClient->setAccessToken($accessToken);
            $cont = $contacts = $apiClient->contacts()->get()->toArray(); 
            $array = [];
            foreach($cont as $contact) {
                $arr = [];
                $arr['name'] = $contact['name'];
                if(isset($contact['custom_fields_values'])) {
                    foreach($contact['custom_fields_values'] as $item) {
                        if($item['field_code'] == 'EMAIL') {
                            $i = 0;
                            foreach($item['values'] as $iter) {
                                $arr['email'][$i] = $iter['value'];
                            }
                        }
                    }
            }
            if(!isset($arr['email'])){
                $arr['email'] = null;
            }
                array_push($array, $arr);
            }
            $response = new JsonResponse($array);
            }
            }
        
        
        return $response ?? new JsonResponse(['ok' => 'ok']);
    }
}
