<?php

declare(strict_types=1);

namespace App\Handler;

use Laminas\Diactoros\Response\HtmlResponse;
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\Diactoros\Response\RedirectResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

use function time;

class ApiHandler implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        
        $clientId = '5aabc54d-7153-400d-89e7-5c118ca8afe5';
        $clientSecret = 'IBzAQNPSw8GwLuGSZvtzZdFEAu2x88m2rpzSiybLAYnbWNaWUZp7rPdqPpaMfAIn';
        $redirectURI = 'https://4c56-212-46-197-210.eu.ngrok.io//api';
        $apiClient = new \AmoCRM\Client\AmoCRMApiClient($clientId, $clientSecret, $redirectURI);
        $state = bin2hex(random_bytes(16));
        if(isset($request->getQueryParams()['referer'])) {
            $apiClient->setAccountBaseDomain($request->getQueryParams()['referer']);
        }
        if(!isset($request->getQueryParams()['code'])) {
            $authorizationUrl = $apiClient->getOAuthClient()->getAuthorizeUrl([
                'state' => $state,
                'mode' => 'post_message',
            ]);
            $response = new RedirectResponse($authorizationUrl);
        } else {
            if(isset($request->getQueryParams()['code'])) {
                $accessToken = $apiClient->getOAuthClient()->getAccessTokenByCode($request->getQueryParams()['code']); 
            }
            
            if ((isset($accessToken)) and (!$accessToken->hasExpired())) {
                $saveToken = [
                    'accessToken' => $accessToken->getToken(),
                    'refreshToken' => $accessToken->getRefreshToken(),
                    'expires' => $accessToken->getExpires(),
                    'baseDomain' => $apiClient->getAccountBaseDomain(),
                ];

            $ownerDetails = $apiClient->getOAuthClient()->getResourceOwner($accessToken);
            $name = $ownerDetails->getName();
            $response = new HtmlResponse("owner name is $name");
            }
        }
        return $response ?? new JsonResponse(['ok' => 'ok']);
    }
}
